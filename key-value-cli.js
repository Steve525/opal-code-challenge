'use strict';

var store = {}
const stdin = process.openStdin();
console.log('Starting application ...');

stdin.addListener('data', function(data) {
  try {
    const inputString = data.toString().replace(/^\s+/g, '').trim();
    const parsedInput = inputString.split(' ');
    const command = parsedInput[0];
    let key, value;

    switch (command) {
      case 'SET':
        if (!inputString.match(/^SET \S+ \S+$/g)) {
          throw new Error('Wrong format for "SET" command. Example: "SET 1bc 2zy"');
        }
        key = parsedInput[1];
        value = parsedInput[2];
        store[key] = value;
        break;
      case 'GET':
        if (!inputString.match(/^GET [a-z0-9]+$/g)) {
          throw new Error('Wrong format for "GET" command. Example: "GET 1bc"');
        }
        key = parsedInput[1];
        value = store[key]
        if (typeof value === 'undefined') {
          throw new Error(`Key not set`)
        }
        console.log(value);
        break;
      case 'DELETE':
        if (!inputString.match(/^DELETE [a-z0-9]+$/g)) {
          throw new Error('Wrong format for "DELETE" command. Example: "DELETE 1bc"');
        }
        key = parsedInput[1];
        if (typeof store[key] === 'undefined') {
          throw new Error(`Key not set`)
        }
        delete store[key];
        break;
      case 'COUNT':
        if (!inputString.match(/^COUNT [a-z0-9]+$/g)) {
          throw new Error('Wrong format for "COUNT" command. Example: "COUNT 1bc"');
        }
        let count = 0;
        value = parsedInput[1]
        for (key in store) {
          if (store.hasOwnProperty(key) && store[key] === value) {
            count++;
          }
        }
        console.log(count);
        break;
      default:
        throw new Error('No valid command given. Valid commands: "GET", "SET", "DELETE", "COUNT"');
    }
  } catch (error) {
    console.log(error.message);
  }
});
